FROM alpine:3.8

ADD https://github.com/houqp/asciidoc-deckjs/releases/download/v1.6.3/deckjs-1.6.3.zip /documents/
RUN apk -q add --no-cache asciidoc && \
    asciidoc --backend install /documents/deckjs-1.6.3.zip

WORKDIR /documents
VOLUME /documents

CMD [ "/bin/sh", "asciidoc-build.sh"]
