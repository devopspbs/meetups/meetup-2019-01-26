#!/bin/sh

mkdir -p ./presentations

asciidoc --backend=deckjs \
         --out-file=presentations/talk-cloud.html \
         talk-cloud.asciidoc \

asciidoc --backend=deckjs \
         --out-file=presentations/meetup-cloud-iot.html \
         meetup-cloud-iot.asciidoc \
	 
exit 0
