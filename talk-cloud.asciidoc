Cloud Computing
===============
:author: Tiago Rocha
:email:
:description:
:copyright: Tiago Rocha 2019
:revdate: 2019-01-26
:revnumber: 0.1
// :max-width: 45em
:data-uri:
:icons:
:deckjs_theme: swiss
:deckjs_transition: fade
//:split:


== Whoami

[incremental="true"]
- *Tiago Rocha*
- Engenheiro de Operações (ou Severino da TI)
- Especialista em Rede de Computadores
- Atualmente trabalho na COOINT (Prefeitura de Parauapebas)
- *Newbie em Cloud Computing*


== O Conceito de Computação em Nuvem

[WARNING]
.*Atenção!*
Não existe consenso quanto ao conceito de Computação em Nuvem.


== Computação em Nuvem (IBM)

Computação em Nuvem, comumente referida simplesmente como "a nuvem", é a entrega de recursos de computação sob demanda - tudo, desde aplicativos até data centers - pela Internet, em uma base de pagamento por uso. (https://www.ibm.com/cloud-computing/br-pt/learn-more/what-is-cloud-computing/[IBM Cloud])


== Computação em Nuvem (Amazon)

A computação em nuvem é a entrega sob demanda de poder computacional, armazenamento de banco de dados, aplicativos e outros recursos de TI por uma plataforma de serviços na nuvem usando a internet com o modelo de definição de preço “pay-as-you-go”. (...)

A computação em nuvem oferece uma maneira simples de acessar servidores, armazenamentos, bancos de dados e uma série de serviços de aplicativos pela internet. (https://d1.awsstatic.com/whitepapers/pt_BR/aws-overview.pdf[Whitepapper da AWS])


== Computação em Nuvem (NIST)

É um Modelo para permitir acesso sob-demanda onipresente e conveniente via rede à um "pool" compartilhado de recursos computacionais configuráveis (ex., rede, servidores, armazenamento, aplicativos e serviços) que podem ser rapidamente provisionados e lançados com o mínimo de esforço de gerenciamento ou interação com o provedor de serviços. (https://ws680.nist.gov/publication/get_pdf.cfm?pub_id=909024[NIST])

// The NIST Definition of Cloud Computing identified cloud computing as a model for enabling ubiquitous, convenient, on-demand network access to a shared pool of configurable computing resources (e.g., networks, servers, storage, applications, and services) that can be rapidly provisioned and released with minimal management effort or service provider interaction. (https://ws680.nist.gov/publication/get_pdf.cfm?pub_id=909024[NIST])


// == Computação em Nuvem ;-)

// image:images/thereisnocloud-v2-74x74.png[height=480,align="center"]


== Modelos de Computação em Nuvem

[incremental="true"]
- *SaaS:* Software as a service
- *PaaS:* Platform as a service
- *IaaS:* Infrastructure as a service


== A Sopa de Letrinhas (*aaS)

Na Wikipedia tem uma https://en.wikipedia.org/wiki/As_a_service[lista bem completa] dos modelos, mas infelizmente os meus preferidos não estão lá ;-)
[incremental="true"]
- *SaaS:* Service as a service
- *PEaaS:* Peão as a service

== Computação em Nuvem

image:images/Cloud_computing.svg[height=480,align="center"]


== Modelo de Implantação

[incremental="true"]
- *Public cloud:* Quando os serviços são apresentados por meio de uma rede que é aberta para uso público.
- *Private cloud:* Aquelas construídas exclusivamente para um único usuário.
- *Hybrid cloud:*  Temos uma composição dos modelos de nuvens públicas e privadas.


== Nuvem Privada =D

image:images/private-cloud.png[height=480]


== Características da Computação em Nuvem

[incremental="true"]
. *On-demand self-service*
. *Broad network access*
. *Resource pooling*
. *Rapid elasticity*
. *Measured service*


== Demo

image:images/8914371209_67ce57f1d9_c.jpg[height=480]


== Obrigado


Dúvidas?
~~~~~~~~


== Contatos

*E-mail:* tiago.rocha at gmx.com

*Telegram:* https://t.me/Tiag0Rocha[@Tiag0Rocha]

*Slack*: tiagorocha

*Riot/Matrix:* tiagorocha

*IRC:* tiagorocha (Freenode e OFTC)

